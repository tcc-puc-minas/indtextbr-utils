const { LoggerService } = require("./services/logger");
const { MailService } = require("./services/mail");
const { AuthService } = require("./middlewares/authorization");

module.exports = {
  AuthService,
  LoggerService,
  MailService,
};
