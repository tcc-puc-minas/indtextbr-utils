const { createLogger, format, transports } = require("winston");
require("winston-daily-rotate-file");

const config = (logName, logLevel) => {
  const transportConsole = new transports.Console({
    level: logLevel,
    silent: process.env.NODE_ENV === "test",
  });

  const transportDailyRotateFile = new transports.DailyRotateFile({
    filename: `logs/${logName}-%DATE%.log`,
    datePattern: "YYYY-MM-DD",
    zippedArchive: true,
    maxFiles: '14d',
    level: logLevel,
  });

  const myTransports = [transportConsole];
  if (process.env.NODE_ENV !== "test") {
    myTransports.push(transportDailyRotateFile);
  }

  const myFormat = format.combine(
    format.timestamp(),
    format.printf(({ timestamp, level, message }) =>
      `[${timestamp}]`.concat(`[${level.toUpperCase()}]: `).concat(message)
    )
  );

  return createLogger({
    format: myFormat,
    transports: myTransports,
  });
};

class Logger {
  constructor({ logName, logLevel = 'info'}) {
    this.logger = config(logName, logLevel);
  }
}

class LoggerService {
  constructor(params) {
    if (global.loggerInstance === undefined) {
      global.loggerInstance = new Logger(params);
    }
  }

  getInstance = () => {
    return global.loggerInstance.logger;
  };
};

module.exports = {
  LoggerService,
  Logger
}
