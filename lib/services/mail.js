const sgMail = require("@sendgrid/mail");

class Mail {
  constructor({ fromMail, apiKey }) {
    this.mailService = sgMail;
    this.from = fromMail;
    this.mailService.setApiKey(apiKey);
  }

  async send({ to, subject, html }) {
    const msg = {
      to, // Change to your recipient
      from: this.from, // Change to your verified sender
      subject,
      html,
    }
    return this.mailService.send(msg);
  }
}

class MailService {
  constructor(params) {
    if (global.mailInstance === undefined) {
      global.mailInstance = new Mail(params);
    }
  }

  getInstance = () => {
    return global.mailInstance;
  };

};

module.exports = {
  MailService,
  Mail
}


