"use-strict";

const jwt = require("jsonwebtoken");
const md5 = require("md5");

class Auth {
  constructor(saltKey) {
    this.saltKey = saltKey;
  }

  generatePassword = (length = 6) => {
    const charset =
      "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    let password = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
      password += charset.charAt(Math.floor(Math.random() * n));
    }
    return password;
  };

  decodeToken = async (token) => {
    return jwt.verify(token, this.saltKey);
  };

  generateToken = async ({ data, expiresIn = "1d" }) => {
    return jwt.sign(data, this.saltKey, { expiresIn: "1d" });
  };

  refreshToken = async (refreshToken) => {
    return await jwt.refreshToken(refreshToken);
  };

  createHash = (pass) => {
    return md5(pass + this.saltKey);
  };

  authorize = async ({ roles, flag }) => {
    if (roles.includes(flag)) {
      return permissionReponse({
        success: true,
        status: 200,
        message: "Usuário autorizado!",
      });
    } else {
      return permissionReponse({
        success: false,
        status: 403,
        message: "Usuário sem acesso a funcionalidade",
      });
    }
  };
}

const permissionReponse = ({ success, status, message }) => {
  return {
    success,
    status,
    message,
  };
};

class AuthService {
  constructor(params) {
    if (global.authInstance === undefined) {
      global.authInstance = new Auth(params.saltKey);
    }
  }

  getInstance() {
    return global.authInstance;
  }
}

module.exports = {
  Auth,
  AuthService,
};
