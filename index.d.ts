declare module "services/logger" {
    export class LoggerService {
        constructor(params: any);
        getInstance: () => any;
    }
    export class Logger {
        constructor({ logName, logLevel }: {
            logName: any;
            logLevel?: string;
        });
        logger: import("winston").Logger;
    }
}
declare module "services/mail" {
    export class MailService {
        constructor(params: any);
        getInstance: () => any;
    }
    export class Mail {
        constructor({ fromMail, apiKey }: {
            fromMail: any;
            apiKey: any;
        });
        mailService: any;
        from: any;
        send({ to, subject, html }: {
            to: any;
            subject: any;
            html: any;
        }): Promise<[import("@sendgrid/client/src/response").ClientResponse, {}]>;
    }

}
declare module "middlewares/authorization" {
    export class Auth {
        constructor(saltKey: any);
        saltKey: any;
        generatePassword: (length?: number) => string;
        decodeToken: (token: any) => Promise<any>;
        generateToken: ({ data, expiresIn }: {
            data: any;
            expiresIn?: string;
        }) => Promise<any>;
        refreshToken: (refreshToken: any) => Promise<any>;
        createHash: (pass: any) => any;
        authorize: ({ roles, flag }: {
            roles: any;
            flag: any;
        }) => Promise<{
            success: any;
            status: any;
            message: any;
        }>;
    }
    export class AuthService {
        constructor(params: any);
        getInstance(): any;
    }
}
declare module "indtextbr-utils" {
    import { AuthService } from "middlewares/authorization";
    import { LoggerService } from "services/logger";
    import { MailService } from "services/mail";
    export { AuthService, LoggerService, MailService };
}
